package com.covalentive.com.deutschebankbpm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class RulesMainActivity extends AppCompatActivity {

    public static void navigate(Context from) {
        Intent rulesMain = new Intent(from, RulesMainActivity.class);
        from.startActivity(rulesMain);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new OnAddRuleRequested());
    }

    private class OnAddRuleRequested implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Snackbar.make(view, "Add Rule", Snackbar.LENGTH_LONG)
                    .setAction("OK", null)
                    .show();
        }
    }
}
