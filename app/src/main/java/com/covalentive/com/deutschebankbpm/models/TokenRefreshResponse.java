package com.covalentive.com.deutschebankbpm.models;

import com.google.gson.annotations.SerializedName;

/**
 * @author Marin Kacaj
 */

public class TokenRefreshResponse {

    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("expires_in")
    private long validityDuration;

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public long getValidityDuration() {
        return validityDuration;
    }
}
