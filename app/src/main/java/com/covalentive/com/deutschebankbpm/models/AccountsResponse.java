package com.covalentive.com.deutschebankbpm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Marin Kacaj
 */

public class AccountsResponse {

    @SerializedName("totalItems")
    private int totalNumItems;
    @SerializedName("limit")
    private int accountsPerPage;
    @SerializedName("offset")
    private int offset;
    @SerializedName("accounts")
    private List<CashAccount> accounts;

    public int getTotalNumItems() {
        return totalNumItems;
    }

    public int getAccountsPerPage() {
        return accountsPerPage;
    }

    public int getOffset() {
        return offset;
    }

    public List<CashAccount> getAccounts() {
        return accounts;
    }
}
