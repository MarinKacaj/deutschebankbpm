package com.covalentive.com.deutschebankbpm.rest;

import com.covalentive.com.deutschebankbpm.models.AccountsResponse;
import com.covalentive.com.deutschebankbpm.models.TokenRefreshResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * @author Marin Kacaj
 */

public interface DeutscheBankApi {

    String AUTH_HDR_NM = "Authorization";

    @GET("gw/dbapi/v2/cashAccounts")
    Call<AccountsResponse> accounts(@Header(AUTH_HDR_NM) String authBearer);

    @POST("gw/oidc/token")
    @FormUrlEncoded
    Call<TokenRefreshResponse> refreshToken(@Header(AUTH_HDR_NM) String authBearer,
                                            @Field("client_id") String clientId,
                                            @Field("client_secret") String clientSecret,
                                            @Field("grant_type") String grantType,
                                            @Field("refresh_token") String refreshToken);
}
