package com.covalentive.com.deutschebankbpm;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.covalentive.com.deutschebankbpm.auth.AuthUtil;

public class AuthRedirectActivity extends AppCompatActivity {

    private TextView resultView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authed);

        resultView = findViewById(R.id.result);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Uri uri = getIntent().getData();
        String authToken = AuthUtil.extractAuthToken(uri);
        AuthUtil.saveAuthToken(this, authToken);

        resultView.setText(R.string.o_auth_2_final_success);
        RulesMainActivity.navigate(this);
    }
}
