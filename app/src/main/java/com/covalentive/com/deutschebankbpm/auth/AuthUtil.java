package com.covalentive.com.deutschebankbpm.auth;

import android.content.Context;
import android.net.Uri;
import android.net.UrlQuerySanitizer;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * @author Marin Kacaj
 */

public class AuthUtil {

    private static final String AUTH_TOKEN_KEY = "authToken";
    private static final String SITE = "https://simulator-api.db.com/gw/oidc/authorize";
    private static final String INVALID_TOKEN = "";

    public static final String CLIENT_ID = "161097a4-3413-48b1-bf3f-65f095dd497f";
    public static final String CLIENT_SECRET = "ALz75JSOIAIyqmh7INLvwgurUvjZy4qrzfzHofyPzwyQ65MxBhVVoVxX5gu9s3y2M4duceiFeujGBynt9_oVimc";
    public static final String TOKEN_REFRESH_GRANT_TYPE = "refresh_token";

    public static Uri authUrl() {
        return Uri.parse(SITE)
                .buildUpon()
                .appendQueryParameter("client_id", CLIENT_ID)
                .appendQueryParameter("response_type", "token")
                .appendQueryParameter("redirect_uri", "http://db-bpm.covalentive")
                .appendQueryParameter("state", Long.toString(System.currentTimeMillis()))
                .build();
    }

    public static String extractAuthToken(Uri postAuthRedirect) {
        String authToken;
        if (postAuthRedirect == null) {
            authToken = null;
        } else {
            String encodedParams = postAuthRedirect.getFragment();
            UrlQuerySanitizer sanitizer = new UrlQuerySanitizer("http://example.com?" + encodedParams); // quick hack
            authToken = sanitizer.getValue("access_token");
        }
        return authToken;
    }

    public static void saveAuthToken(Context context, String authToken) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(AUTH_TOKEN_KEY, authToken)
                .apply();
    }

    @Nullable
    public static String getAuthToken(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(AUTH_TOKEN_KEY, null);
    }

    public static String authBearer(Context context) {
        return " Bearer " + getAuthToken(context);
    }

    public static void invalidateToken(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(AUTH_TOKEN_KEY, INVALID_TOKEN)
                .apply();
    }

    public static boolean isCurrentTokenInvalid(Context context) {
        String currentAuthToken = getAuthToken(context);
        return TextUtils.isEmpty(currentAuthToken) || INVALID_TOKEN.equals(currentAuthToken);
    }
}
