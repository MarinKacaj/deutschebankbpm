package com.covalentive.com.deutschebankbpm.models;

import com.google.gson.annotations.SerializedName;

/**
 * @author Marin Kacaj
 */

public class CashAccount {

    @SerializedName("iban")
    private String iban;
    @SerializedName("currencyCode")
    private String currencyCode;
    @SerializedName("bic")
    private String bic;
    @SerializedName("accountType")
    private String accountTypeName;
    @SerializedName("currentBalance")
    private float currentBalance;
    @SerializedName("productDescription")
    private String productDescription;

    public String getIban() {
        return iban;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getBic() {
        return bic;
    }

    public String getAccountTypeName() {
        return accountTypeName;
    }

    public float getCurrentBalance() {
        return currentBalance;
    }

    public String getProductDescription() {
        return productDescription;
    }
}
