package com.covalentive.com.deutschebankbpm;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.covalentive.com.deutschebankbpm.auth.AuthUtil;
import com.covalentive.com.deutschebankbpm.models.AccountsResponse;
import com.covalentive.com.deutschebankbpm.models.TokenRefreshResponse;
import com.covalentive.com.deutschebankbpm.rest.DeutscheBankApi;
import com.covalentive.com.deutschebankbpm.rest.ServiceGenerator;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LauncherActivity extends AppCompatActivity {

    private TextView reportView;
    private Button retryButton;
    private DeutscheBankApi deutscheBankApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        reportView = findViewById(R.id.report);
        retryButton = findViewById(R.id.retry_oauth);

        retryButton.setOnClickListener(new OnRetryRequested());

        deutscheBankApi = ServiceGenerator.createService(DeutscheBankApi.class);
    }

    @Override
    protected void onResume() {
        super.onResume();

        startAuth();
    }

    private void triggerLogin() {
        Uri authUrl = AuthUtil.authUrl();
        Intent login = new Intent(Intent.ACTION_VIEW, authUrl);
        startActivity(login);
    }

    private void goToMain() {
        RulesMainActivity.navigate(this);
    }

    private void retrieveAccounts(String authBearer) {
        deutscheBankApi.accounts(authBearer).enqueue(new AccountsResponseCallback());
    }

    private void startAuth() {
        reportWithoutRetry(R.string.authentication_in_progress);
        if (AuthUtil.isCurrentTokenInvalid(this)) {
            triggerLogin();
        } else {
            retrieveAccounts(AuthUtil.authBearer(this));
        }
    }

    private void reportErrorWithRetry(@StringRes int messageId) {
        reportView.setText(messageId);
        retryButton.setVisibility(View.VISIBLE);
        retryButton.setEnabled(true);
    }

    private void reportWithoutRetry(@StringRes int messageId) {
        reportView.setText(messageId);
        retryButton.setVisibility(View.GONE);
        retryButton.setEnabled(false);
    }

    private class AccountsResponseCallback implements Callback<AccountsResponse> {

        @Override
        public void onResponse(Call<AccountsResponse> call, Response<AccountsResponse> response) {
            if (response.isSuccessful()) {
                reportWithoutRetry(R.string.o_auth_2_final_success);
                goToMain();
            } else if (response.code() >= HttpURLConnection.HTTP_BAD_REQUEST
                    && response.code() < HttpURLConnection.HTTP_INTERNAL_ERROR) {
                String currentAuthToken = AuthUtil.getAuthToken(LauncherActivity.this);
                String currentAuthBearer = AuthUtil.authBearer(LauncherActivity.this);
                deutscheBankApi.refreshToken(currentAuthBearer, AuthUtil.CLIENT_ID, AuthUtil.CLIENT_SECRET,
                        AuthUtil.TOKEN_REFRESH_GRANT_TYPE, currentAuthToken)
                        .enqueue(new TokenRefreshResponseCallback());
            } else {
                reportErrorWithRetry(R.string.accounts_retrieval_unknown_error);
            }
        }

        @Override
        public void onFailure(Call<AccountsResponse> call, Throwable t) {
            reportErrorWithRetry(R.string.could_not_connect_to_accounts_endpoint);
        }
    }

    private class TokenRefreshResponseCallback implements Callback<TokenRefreshResponse> {

        @Override
        public void onResponse(Call<TokenRefreshResponse> call, Response<TokenRefreshResponse> response) {
            TokenRefreshResponse tokenRefreshResponse = response.body();
            if (null == tokenRefreshResponse || !response.isSuccessful()) {
                AuthUtil.invalidateToken(LauncherActivity.this);
                reportErrorWithRetry(R.string.unable_to_acquire_auth_token);
            } else {
                AuthUtil.saveAuthToken(LauncherActivity.this, tokenRefreshResponse.getAccessToken());
                retrieveAccounts(AuthUtil.authBearer(LauncherActivity.this));
            }
        }

        @Override
        public void onFailure(Call<TokenRefreshResponse> call, Throwable t) {
            reportErrorWithRetry(R.string.unable_to_acquire_auth_token);
        }
    }

    private class OnRetryRequested implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            startAuth();
        }
    }
}
